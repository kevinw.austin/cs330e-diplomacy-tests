from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):


    def test_Diplomacy_solve_1(self):
        r = StringIO("A Paris Move London\nB London Move Berlin\nC Berlin Move Paris\n") #Everyone Moves Corner Case
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Berlin\nC Paris\n")

    def test_Diplomacy_solve_2(self):
        r = StringIO("A Rome Move Berlin\nB London Move Berlin\nC Berlin Hold\nD Moscow Support B\nE Kiev Move Moscow\n")
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\n")

    def test_Diplomacy_solve_3(self):
        r = StringIO("A London Hold\nB Berlin Hold\nC Paris Hold\nD Moscow Hold\nE Kiev Hold\n") #Everyone Holds Corner Case
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Berlin\nC Paris\nD Moscow\nE Kiev\n")

    def test_diplomacy_read_1(self):
        y =  "A London Hold\n"
        val = diplomacy_read(y)
        self.assertEqual(val, ["A", "London", "Hold"])

# ----
# main
# ----

if __name__ == "__main__":
    main()
