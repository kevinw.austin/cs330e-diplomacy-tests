#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# Testdiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1], "Madrid")
        self.assertEqual(i[2], "Hold")
    
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "B")
        self.assertEqual(i[1], "Barcelona")   
        self.assertEqual(i[2], "Move")
        self.assertEqual(i[3], "Madrid")
        
        s = "C London Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "C")
        self.assertEqual(i[1], "London")   
        self.assertEqual(i[2], "Move")
        self.assertEqual(i[3], "Madrid")
        
        s = "D Paris Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "D")
        self.assertEqual(i[1], "Paris")   
        self.assertEqual(i[2], "Support")
        self.assertEqual(i[3], "B")
        
        s = "E Austin Support A\n"
        i =  diplomacy_read(s)
        self.assertEqual(i[0], "E")
        self.assertEqual(i[1], "Austin")   
        self.assertEqual(i[2], "Support")
        self.assertEqual(i[3], "A")

    def test_read2(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1], "Madrid")
        self.assertEqual(i[2], "Hold")
    
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "B")
        self.assertEqual(i[1], "Barcelona")   
        self.assertEqual(i[2], "Move")
        self.assertEqual(i[3], "Madrid")
        
        s = "C London Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "C")
        self.assertEqual(i[1], "London")   
        self.assertEqual(i[2], "Support")
        self.assertEqual(i[3], "B")
        
        s = "D Austin Move London\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "D")
        self.assertEqual(i[1], "Austin")   
        self.assertEqual(i[2], "Move")
        self.assertEqual(i[3], "London")

    def test_read3(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1], "Madrid")
        self.assertEqual(i[2], "Hold")
    
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "B")
        self.assertEqual(i[1], "Barcelona")   
        self.assertEqual(i[2], "Move")
        self.assertEqual(i[3], "Madrid")
        
        s = "C London Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0], "C")
        self.assertEqual(i[1], "London")   
        self.assertEqual(i[2], "Move")
        self.assertEqual(i[3], "Madrid")

    # ----
    # eval
    # ----

    def test_eval(self):
        
        a = [["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Move", "Madrid"],["D", "Paris", "Support", "B"],["E", "Austin", "Support", "A"]]
        b = diplomacy_eval(a)
        for i in range(len(b)):
            # for j in range(len(b[i])):
            if i == 0:
                self.assertEqual(b[i][0], "A")
                self.assertEqual(b[i][1], "[dead]")
            if i == 1:
                self.assertEqual(b[i][0], "B")
                self.assertEqual(b[i][1], "[dead]")
            if i == 2:
                self.assertEqual(b[i][0], "C")
                self.assertEqual(b[i][1], "[dead]")
            if i == 3:
                self.assertEqual(b[i][0], "D")
                self.assertEqual(b[i][1], "Paris")
            if i == 4:
                self.assertEqual(b[i][0], "E")
                self.assertEqual(b[i][1], "Austin")
                

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        a = [["A","[dead]"],["B", "[dead]"],["C","[dead]"], ["D", "Paris"], ["E", "Austin"]]
        diplomacy_print(w, a)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    


    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
             w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
             
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
             w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve3(self):
         r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
         w = StringIO()
         diplomacy_solve(r, w)
         self.assertEqual(
             w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


<<<<<<< HEAD
$ coverage report -m                   >> TestDiplomacy.out
=======
$ coverage report -m                   >> Testdiplomacy.out
>>>>>>> 553c3b93c8a54cbfbc12a3f4e1477d6efeffeede



$ cat TestDiplomacy.out
........
----------------------------------------------------------------------
Ran 8 tests in 0.004s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          94     18     64      9    78%   66->67, 67, 78->63, 79->63, 116->132, 118->125, 120->124, 124, 125->126, 126-127, 132-133, 136->158, 137->138, 138-151
TestDiplomacy.py     117      0     12      0   100%
--------------------------------------------------------------
TOTAL                211     18     76      9    88%

"""
